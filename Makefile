OGDF_INCLUDE_D := ./lib/OGDF/include/
CC = g++
CXX = g++

EXEC = tp1

LDLIBS := -lemon

OUTPUTS := $(wildcard *.cpp)
OBJ := $(patsubst %.cpp, %.o, $(wildcard *.cpp))

all:
	$(CC) $(CPPFLAGS) $(OUTPUTS) -o $(EXEC)

clean:
	rm a.out *.o
