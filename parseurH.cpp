#include "planete.h"

#include <cassert>
#include <cstdlib>
#include <istream>
#include <sstream>
#include <stack>

bool getInteger(istream&, Planet&);

istream& operator >> (istream &is, Planet &p){
	if(!getInteger(is, p)) return is;

	string line;

	while(getline(is, line)){
		if((int)line.back() == 13)
			line.pop_back();
		for(int i = 0; i < line.size(); i++)
			p.addNode(line[i]);
	}
	return is;
}

bool getInteger(istream &is, Planet &p){
	string line;
	stack<int> numbers;

	for(int i = 0; i < 2; i++){
		getline(is, line);
		istringstream input(line);
		int dimension;

		if(!(input >> dimension))
			return false;

		numbers.push(dimension);
	}

	p.setWidth(numbers.top());
	numbers.pop();
	p.setHeight(numbers.top());

	return true;
}