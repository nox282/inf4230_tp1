#include <iostream>
#include <fstream>
#include <ctime>

#include "planete.h"
#include "pathFinder.h"

ostream &operator<<(ostream &os, Position &p){
	os << p.y << '-' << p.x;
	return os;
}

void displayStep(stack<int> p, int w){
	if(!p.empty()){
		Position current = Position(p.top(), w);
		Position next;
		p.pop();

		while(!p.empty()){
			next = Position(p.top(), w);
			Position move = next - current;
				 if(move.x ==  1)cout << "Est = ";
			else if(move.x == -1)cout << "Ouest = ";
			else if(move.y ==  1)cout << "Sud = ";
			else if(move.y == -1)cout << "Nord = ";

			cout << "Lieu " << current << " -> Lieu " << next << "); " << endl;

			current = next;
			p.pop();
		}
	}
}

void pahtOutput(Planet &p, stack<int> pathToBomb, stack<int> pathToEnd){
	cout << "Plan  { " << endl;
	displayStep(pathToBomb, p.getWidth());
	cout << "Charger(r0,B); " << endl;
	displayStep(pathToEnd, p.getWidth());
	cout << "Decharger(r0,B); " << endl;
	cout << "}";
}

int getCost(stack<int> path, Planet &p){
	Rover rover(p);
	int ret;
	while(!path.empty()){
		ret += rover[path.top()];
		path.pop();
	}
	return ret;
}

void displayCost(stack<int> pathToBomb, stack<int> pathToEnd, Planet &p){
	cout << "3) cout : " << (getCost(pathToBomb, p) + getCost(pathToEnd, p) + 60) << endl;
	
}

int main (int argc, const char **argv){

	cout <<"Donne le nom du fichier contenant ton mondeH." << endl
	<< "Remarque: Le programme s'execute par defaut avec une heuristique de Manhattan. Pour l'executer avec une heureustic Euclidean" << endl
	<< "executer avec ./tp1 avec l'option -E => `./tp1 -E`" << endl	
	<< "Remarque2: le fichier doit être à la racine du projet et dois se terminer par .txt" << endl << "Exemple: planeteH01.txt" << endl << endl;

	Planet p;

	string input;
	cin >> input;

	ifstream isplanet(input);

	if(isplanet.fail()){
		cerr << "Erreur d'ouverture du fichier: '" << argv[1] << "' !" << endl;
		return 1;
	}

	clock_t start = clock();

	isplanet >> p;

	stack<int> pathToBomb;
	stack<int> pathToEnd;

	if(argc != 0){
		pathToBomb = aStar(p, p.getStartIndex(), p.getBombIndex(), Euclidean(p[p.getBombIndex()]), "debut a bombde");
		pathToEnd = aStar(p, p.getBombIndex(), p.getEndIndex(), Euclidean(p[p.getEndIndex()]), "bombe a fin");
	} else {
		pathToBomb = aStar(p, p.getStartIndex(), p.getBombIndex(), Manhattan(p[p.getBombIndex()]), "debut a bombde");
		pathToEnd = aStar(p, p.getBombIndex(), p.getEndIndex(), Manhattan(p[p.getEndIndex()]), "bombde a fin");
	}
	if(pathToBomb.empty() || pathToEnd.empty())
		cout << "Pas de chemin trouve." << endl;
	else{
		double duration =  (clock() - start) / (double) CLOCKS_PER_SEC;
		displayCost(pathToBomb, pathToEnd, p);
		cout << "4) temps (seconde) : " << duration << endl << endl;
		pahtOutput(p, pathToBomb, pathToEnd);
	}
	return 0;
}