#if !defined (PATH_FINDER_H)
#define PATH_FINDER_H

#include "planete.h"
#include <stack>
#include <map>
#include <set>
#include <limits>
#include <cmath>

class Heuristic{
	protected:
		Position end;
	public:
		Heuristic(Position e):end(e){};
		virtual int estimate (const Position index) const{return 0;};
};

class Manhattan: public Heuristic{
	public:
		Manhattan(Position e):Heuristic(e){};
		int estimate(const Position index) const{
			return abs(end.x - index.x) + abs(end.y - index.y);
		};
};

class Euclidean: public Heuristic{
	public:
		Euclidean(Position e):Heuristic(e){};
		int estimate(const Position index) const{
			return sqrt((end.x - index.x)*(end.x - index.x) + (end.y - index.y)*(end.y - index.y));
		};
};

stack<int> aStar(Planet&, const int start, const int end, const Heuristic &h, string);

#endif