#include "pathFinder.h"

stack<int> reconstructPath(set<int> closedSet, map<int, int> parent, int goal, int start){

	stack<int> path;
	set<int>::iterator it = closedSet.find(goal);
	
	while(*it != start){
		path.push(*it);
		it = closedSet.find(parent[*it]);
	}
	path.push(start);
	return path;
}

int findNextPriority(auto fScore, set<int> openSet){
	for(set<pair<int, int> >::iterator it = fScore.begin(); it != fScore.end(); it++){
		if(openSet.find(it->first) != openSet.end())
			return it->first;
	}

	return fScore.begin()->first;
}

stack<int> aStar(Planet &p, const int start, const int goal, const Heuristic &h, string action){
	Rover rover(p);
	int generatedNodes;

	class Compare{
		public: bool operator() (const pair<int, int> &p1, const pair<int, int> &p2) const{return p1.second < p2.second;};
	};

	set<int> openSet;
	set<int> closedSet;

	map<int, int> cameFrom;
	map<int, int> gScore;

	multiset<pair<int, int>, Compare> fScore;

	openSet.insert(start); generatedNodes++;

	for(int i = 0; i < p.size(); i++)
		gScore[i] = numeric_limits<int>::max();

	gScore[start] = 0;
	fScore.insert(pair<int, int>(start, h.estimate(p[start])));
	
	while(!openSet.empty()){

		int current = findNextPriority(fScore, openSet);
				
		if (current == goal){
			closedSet.insert(current);
			cout << "1) Nombre de noeuds generes pour " << action << " : " << generatedNodes << endl;
			cout << "2) Nombre de noeuds visites " << action << " : " << closedSet.size() << endl << endl;

			return reconstructPath(closedSet, cameFrom, goal, start);
		}

		openSet.erase(current);
		closedSet.insert(current);

		vector<int> neighbors = rover.getNeighbors(current);

		for(int i = 0; i < neighbors.size(); i++){
			int n_index = neighbors[i];

			if(closedSet.find(n_index) == closedSet.end()){

				int tentative_gscore = gScore[current] + rover[n_index];
				
				if(openSet.find(n_index) == openSet.end()) {
					openSet.insert(n_index);
					generatedNodes++;
				}

				if(tentative_gscore <= gScore[n_index]){
				
					cameFrom[n_index] = current;
					gScore[n_index] = tentative_gscore;
					fScore.insert(pair<int, int>(n_index, (tentative_gscore + h.estimate(p[n_index]))));
				}
			}
		}
	}
	
	cout << "1) Nombre de noeuds generes pour " << action << " : " << generatedNodes << endl;
	cout << "2) Nombre de noeuds visites " << action << " : " << closedSet.size() << endl << endl;

	return stack<int>();
}