#if !defined (PLANET_H)
#define PLANET_H

#include <istream>
#include <iostream>
#include <vector>
#include <stack>

using namespace std;

struct Position {
	int x;
	int y;

	Position():x(0), y(0){};
	Position(int index, int w):x(index % w), y(index / w){};
	Position(pair<int, int> pos):x(pos.first), y(pos.second){};
	
	int toOneD(int w){ return y * w + x;};

	friend ostream &operator<<(ostream &os, Position &p);

	Position operator-(Position &other){
		return Position(make_pair(
				x - other.x,
				y - other.y
			));
	}
};


/*
 * This is Planet which contains each nodes in a 1D vector
 */
class Planet{
	private:
		int width, height;

		int startIndex;
		int bombIndex;
		int endIndex;
		
		vector<char> board;
		
	public:
				
		Planet():width(0), height(0) {};
		Planet(int h, int w):width(w), height(h){};

		void setWidth(int w){width = w;};
		void setHeight(int h){height = h;};

		int getStartIndex(){ return startIndex;};
		int getBombIndex(){ return bombIndex;};
		int getEndIndex(){ return endIndex;};

		int getWidth() const{return width;};


		void addNode(char type){
			board.push_back(type);
			switch(type){
				case 'E':
					startIndex = board.size()-1;
					break;
				case 'B':
					bombIndex = board.size()-1;
					break;
				case 'S':
					endIndex = board.size()-1;
					break;
			}
		};

		//returns the 2D position of a node
		const Position operator[] (int index) const {return Position(index, width);}; 

		char getNodeType(int index){return board[index];};
		int size() const {return board.size();};

		void draw(){
			for(int i = 0; i < height; i++){
				for(int j = 0; j < width; j++){
					cout << getNodeType(i * width + j);
				}
				cout << endl;
			}
		}

	friend istream& operator >> (istream &is, Planet &p);
	friend class Rover;
};


/*
 * This a Rover that can move on a planet. Constructors places it at start position of the planet
 */
class Rover{
	private:
		Planet planet;

		bool isMoveLegal(int destination){		//prevents out of map and illegal move
			return !(destination < 0 || 
				   	destination > planet.size() ||
					planet.getNodeType(destination) == ' '
					);
		};

		stack<int> getSurroudingNodes(int index){ //returns surrounding Nodes that are inside the world
			stack<int> q;
			Position pos = Position(index, planet.width);
			if(pos.y-1 >= 0			   ) q.push(Position(make_pair(pos.x  , pos.y-1)).toOneD(planet.width));
			if(pos.y+1 <  planet.height) q.push(Position(make_pair(pos.x  , pos.y+1)).toOneD(planet.width));
			if(pos.x-1 >= 0			   ) q.push(Position(make_pair(pos.x-1, pos.y  )).toOneD(planet.width));
			if(pos.x+1 <  planet.width ) q.push(Position(make_pair(pos.x+1, pos.y  )).toOneD(planet.width));

			return q;
		};
	public:
		Rover(Planet p):planet(p){};

		vector<int> getNeighbors(int index){ //returns the neighbors which the rover can move to
			vector<int> ret;
			stack<int> q = getSurroudingNodes(index);
			
			while(!q.empty()){
				if(isMoveLegal(q.top()))
					ret.push_back(q.top());
				q.pop();
			}

			return ret;
		}

		int operator[] (int index){ 	//returns the cost to move to [index]
			switch(planet.getNodeType(index)){
				case 'E':
					return 2;
					break;
				case 'B':
					return 2;
					break;
				case 'S':
					return 2;
					break;
				case '#':
					return 2;
					break;
				case '+':
					return 1;
					break;
				case '-':
					return 3;
					break;
				case 'P':
					return 3;
					break;
			}
		}
};


#endif